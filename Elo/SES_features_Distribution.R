library(tidyverse)
library(dplyr)
library(ggplot2)
library(modelr)
library(broom)
library(DT)
library(skimr)
library(magrittr)
library(ggpubr)
library(RColorBrewer)
library(gplots)
library(ggsignif)
library(ggrepel)
library(gridExtra)
library(rstatix)
library(emmeans)
library(hrbrthemes)
library(GGally)
library(viridis)
library(lazyWeave)
library(cowplot)
library(ggpubfigs)

ito_seven <- friendly_pal("ito_seven") # ggpubfigs
pal_okabe_ito <- ito_seven[c(6,5,3,7,1,2,4)] # order of Wilke

setwd("/Path/to/directory")

NanoString_df <- read.csv("./Data/LabExMI_Nanostring_9Stim_925Donors_Data_2019_12_03_ELO_LogTransformed.tsv", 
                          sep = '\t', 
                          header=T)

to_test <- NanoString_df %>% filter(stimuli.nr == "Null")

to_test <- to_test %>% arrange(ELO)

# Creating a categorical variable for SES (low, mid and high SES)
for (i in 1:nrow(to_test)){
  if (i <= nrow(to_test) / 3){
    to_test[i, "Rank"] <- "Low"
  }
  else if ( (i <= 2 * nrow(to_test) / 3) && (i > nrow(to_test) / 3) ){
    to_test[i, "Rank"] <- "Mid"
  }
  else {
    to_test[i, "Rank"] <- "High"
  }
}

# Creating colors "dictionary" to plot categorical variables
colors.rank <- c("Low" =  "#E69F00", 
                 "Mid" = "#56B4E9",
                 "High"= "#009E73"
                 )

colors.age <- c("20-29" = "#5D0D6E",
                "30-39" = "#ED4353", 
                "40-49" = "#FED440",
                "50-59" = "#3CCDB4", 
                "60-69" = "#009623")

colors.income <- c("From 0 To 1000" = "#4F0002",
                   "From 1001 To 2000" = "#720019", 
                   "From 2001 To 3000" = "#CF4146",
                   "From 3001 To 4000" = "#FF9C54", 
                   "From 4001 To 5000" = "#FFC400", 
                   "Over 5000" = "#FFF200")

colors.education <- c("No Diploma" = "#F62992", 
                      "Primary School" = "#6B0AB8", 
                      "Lower than High School" = "#340DA4",
                      "High School Diploma" = "#476AED", 
                      "Technical Degree" = "#50CFEF", 
                      "Third level & more" = "#0DFFD3")

colors.housing <- c("LEASER" = "#038A91", 
                    "OWNER" = "#03C490")

to_test <- to_test %>% relocate(Rank, .after = ELO) %>% arrange(SUBJID)

to_test$Rank <- factor(to_test$Rank, levels = c("Low", "Mid", "High"))

# Rank distribution

plt_rank <- to_test %>%  
  ggplot( aes(x = ELO)) +
  geom_histogram(aes(y = ..density.., fill = Rank), position="identity", alpha=0.7, bins = 20) +
  #geom_density() +
  scale_fill_manual(values = colors.rank) +
  scale_fill_manual(values = pal_okabe_ito) +
  ylab("Density") +
  theme_pubr() + 
  theme(axis.text = element_text(size = 8),
        axis.title = element_text(size = 10),
        legend.text = element_text(size = 6),
        legend.title = element_text(size = 10))
# pdf("./Plots/SES_features/EloDistribution_Threshold.pdf", width=8, height=5)
# dev.size(units = 'in')
# print(plt_rank)
# dev.off()
plt_rank

# Age distribution

plt_dens_age <- to_test %>%  
  ggplot( aes(x = ELO, color=AGE)) +
  # geom_histogram(aes(y = ..density..), position="identity", alpha=0.7, bins = 20) +
  geom_density() +
  scale_color_manual(values = colors.age) +
  # scale_fill_manual(values = pal_okabe_ito) +
  theme_pubr() +
  ylab("Density")+ 
  theme(axis.text = element_text(size = 8),
        axis.title = element_text(size = 10),
        legend.text = element_text(size = 6),
        legend.title = element_text(size = 10))
pdf("./Plots/SES_features/EloDistribution_density_Age.pdf", width=6, height=4)
dev.size(units = 'in')
print(plt_dens_age)
dev.off()
plt_dens_age

## Sex Distribution

plt_sex <- to_test %>%  
  ggplot( aes(x = ELO, color=SEX)) +
  # geom_histogram(aes(y = ..density..), position="identity", alpha=0.7, bins = 20) +
  geom_density() +
  # scale_fill_manual(values = c("female" = "brown2", "male" = "cornflowerblue")) +
  scale_color_manual(values = c("female" = "#CD534CFF", "male" = "#0073C2FF")) +
  theme_pubr()+ 
  theme(axis.text = element_text(size = 8),
        axis.title = element_text(size = 10),
        legend.text = element_text(size = 6),
        legend.title = element_text(size = 10))
# pdf("./Plots/SES_features/EloDistribution_density_SEX.pdf", width=8, height=5)
# dev.size(units = 'in')
# print(plt_sex)
# dev.off()
plt_sex

plt_bp_sex <- to_test %>%  
  ggplot( aes(x = SEX, y=ELO, color = SEX)) +
  # geom_histogram(aes(y = ..density..), position="identity", alpha=0.7, bins = 20) +
  geom_boxplot() +
  geom_jitter(alpha = 0.5) +
  # scale_fill_manual(values = c("female" = "brown2", "male" = "cornflowerblue")) +
  ggsignif::geom_signif(
    test = "wilcox.test",
    comparisons = list(c("female", "male")),
    map_signif_level = TRUE, 
    y_position = c(2500),
    tip_length = c(0,0),
    color = "black") +
  scale_color_manual(values = c("female" = "#CD534CFF", "male" = "#0073C2FF")) +
  theme_pubr()+ 
  theme(axis.text = element_text(size = 8),
        axis.title = element_text(size = 10),
        legend.text = element_text(size = 6),
        legend.title = element_text(size = 10))
# pdf("./Plots/SES_features/EloBoxplot_SEX.pdf", width=7, height=7)
# dev.size(units = 'in')
# print(plt_bp_sex)
# dev.off()
plt_bp_sex

plist <- list(plt_rank,
              plt_dens_age, 
              plt_bp_sex
             )
### combining the previous plots
plt <- plot_grid(plt_rank,
                 plt_dens_age, 
                 plt_bp_sex, 
                 ncol = 3, nrow = 1, 
                 align = "vh", 
                 axis = "lb", 
                 rel_widths = c(1,1,0.7))

# # Eventually save it as pdf
# pdf(paste0("./Plots/SES_features/EloDistribution_Rank_Sex_Age.pdf"), width = 11, height = 4)
# dev.size(units = "cm")
# print(plt)
# dev.off()

# Distribution in the MI cohort of SES features

to_test_education <- to_test
to_test_education$Education <- factor(to_test_education$Education,
                                      levels = c("No Diploma", "Primary School", "Lower than High School",
                                                 "High School Diploma", "Technical Degree", "Third level & more"))
## education attainment 
plt_education <- to_test_education %>% 
                  ggplot(aes(x = AGE, fill = Education)) +
                  scale_fill_manual(values = colors.education) +
                  geom_bar(aes(y = (..count..)/sum(..count..)), position = "dodge") +
                  theme_classic() + 
                  ylab("Prop") + 
                  theme(legend.position = "top",
                        axis.title = element_text(size = 12),
                        axis.text = element_text(size = 12),
                        legend.text = element_text(size = 9))
plt_education
ggsave("./Plots/SES_features/Age_distribution_education.pdf", width = 6, height = 4)

## income  
plt_income <- to_test %>% 
  ggplot(aes(x = AGE, fill = Income)) +
  scale_fill_manual(values = colors.income) +
  geom_bar(aes(y = (..count..)/sum(..count..)), position = "dodge") +
  theme_classic() + 
  ylab("Prop") +
  theme(legend.position = "top",
        axis.title = element_text(size = 12),
        axis.text = element_text(size = 12),
        legend.text = element_text(size = 9))
plt_income
ggsave("./Plots/SES_features/Age_distribution_income.pdf", width = 6, height = 4)
# ggsave("./Plots/SES_features/SES_features_distribution/Income_Age_Distribution.pdf", width = 6, height = 4)

## housing status
to_test_housing <- to_test %>% 
                    mutate(Housing = ifelse(Housing == "LANDLORD", "OWNER", Housing))

plt_housing <- to_test_housing %>% 
  ggplot(aes(x = AGE, fill = Housing)) +
  scale_fill_manual(values = colors.housing) +
  geom_bar(aes(y = (..count..)/sum(..count..)), position = "dodge") +
  theme_classic() + 
  ylab("Prop") + 
  theme(legend.position = "top",
        axis.title = element_text(size = 12),
        axis.text = element_text(size = 12),
        legend.text = element_text(size = 9))
plt_housing
ggsave("./Plots/SES_features/Age_distribution_housing.pdf", width = 6, height = 4)
# ggsave("./Plots/SES_features/SES_features_distribution/Housing_Age_Distribution.pdf", width = 6, height = 4)

