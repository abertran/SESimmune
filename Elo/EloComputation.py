import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
from scipy.stats import norm
from random import seed
from datetime import date

path_to_data = "/Path/to/Data/"

# Loading the dataset with info on education, income and household
NanoString_df = pd.read_csv(path_to_data + "20201221_MI_SES_Metadata.csv", sep = ",")

NanoString_df.dropna(subset = ["LOG"], inplace=True)
#print(NanoString_df.shape)
NanoString_df.dropna(subset = ["NIVETUD"], inplace=True)
#print(NanoString_df.shape)
NanoString_df.dropna(subset = ["REVENUS"], inplace=True)
#print(NanoString_df.shape)
Nsamples = NanoString_df.shape[0]

colnames = [col for col in NanoString_df.columns]

print(NanoString_df)

tmp = NanoString_df.to_numpy()

NanoString_df = pd.DataFrame(tmp, columns = colnames)

print(NanoString_df)

education_dict = {"NoDiploma" : 0, 
                "CertificatEtudesPrimaires" : 1,
                "CAP_BEP_BrevetDesColleges" : 2,
                "Baccalaureat" : 3,
                "DUT_BTS_DEUG_L2" : 4,
                "L3_M1_M2_PhD" : 5
}

housing_dict = {"LEASER" : 0,
                "LANDLORD" : 1
}

income_dict = {"From_0_To_1000" : 0, 
               "From_1001_To_2000" : 1,
               "From_2001_To_3000" : 2,
               "From_3001_To_4000" : 3,
               "From_4001_To_5000" : 4,
               "Over_5000" : 5
}

def match(individual1, individual2, data = NanoString_df):
    """
    Return the outcome a the match between 2 individuals from the meta data
    based on the comparisons of their SES features.
    It compares each individuals on their :
        - Education (`NIVETUD`) 
        - Incomes (`REVENUS`) 
        - Housing (`LOG`)
    For each of these features, get one point if one is superior to the other
    individual's feature. The losere of the comparison of a feature gets 0 points
    Then it sums all the point that each candidates got.
    The one with the higher sum value wins the match.

    Arguments : 
        individual1 : (int) the row index of the corresponding individual
        individual2 : (int) the row index of the corresponding opponent
        data : (pandas.DataFrame) data stocking the SES features for the individuals

    Values : 
        game : (ndarray) Array of shape (1,2) containing where each colulmn correpsonds
               to the sum of points earned by each individual. The winner has the highest value.
    """

    bo = np.zeros((3,2))

    if education_dict.get(data.loc[individual1, "NIVETUD"]) > education_dict.get(data.loc[individual2, "NIVETUD"]):
        map1 = np.array([1., 0.])
    elif education_dict.get(data.loc[individual1, "NIVETUD"]) < education_dict.get(data.loc[individual2, "NIVETUD"]):
        map1 = np.array([0., 1.])
    else :
        map1 = np.array([0., 0.])
    
    if income_dict.get(data.loc[individual1, "REVENUS"]) > income_dict.get(data.loc[individual2, "REVENUS"]):
        map2 = np.array([1., 0.])
    elif income_dict.get(data.loc[individual1, "REVENUS"]) < income_dict.get(data.loc[individual2, "REVENUS"]):
        map2 = np.array([0., 1.])
    else :
        map2 = np.array([0., 0.])

    if housing_dict.get(data.loc[individual1, "LOG"]) > housing_dict.get(data.loc[individual2, "LOG"]):
        map3 = np.array([1., 0.])
    elif housing_dict.get(data.loc[individual1, "LOG"]) < housing_dict.get(data.loc[individual2, "LOG"]):
        map3 = np.array([0., 1.])
    else :
        map3 = np.array([0., 0.])
    
    bo[0,:] = map1
    bo[1,:] = map2
    bo[2,:] = map3
    
    game = np.sum(bo, axis = 0)

    return game

print(match(0, 1))

def sigmoid(x):
    """
    Take an int, float or ndarray and return the function :
    $$ f(x) = \\frac{1}{1 + 10^{-0.0025x}} $$ 

    Arguments : 
        x : (int, float or ndarray) will correspond to the elo
            difference between 2 individuals in the function
            win_proba 
    
    Values : 
        y = f(x) according to the formula : 
            $$ f(x) = \\frac{1}{1 + 10^{-0.0025x}} $$ 
    """
    return (1 / (1 + np.exp(np.log(10) * (-0.0025 * x))))

# test_sigmoid = np.linspace(-800, 800, 1600)
# f_sigmoid = sigmoid(test_sigmoid)
# plt.plot(test_sigmoid, f_sigmoid)
# plt.show()

def win_proba(individual1, individual2, duel_step, elo_mat):
    """
    Return the probability of individual1 to win over individual 2 regarding their
    current Elo ranking.

    Arguments:
        individual1 : (int) the row index of the corresponding individual
        individual2 : (int) the row index of the corresponding opponent
        duel_step : (int) the number of the current duel
        elo_mat : (ndarray) the matrix stocking the previous individuals' elo
    
    Values: 
        p : (float) Number between [0.,1.] corresponding to the probability of individual1
            to win over individual2 regarding their previous elo.
    """

    elo_ind1 = elo_mat[individual1, duel_step - 1]
    elo_ind2 = elo_mat[individual2, duel_step - 1]

    diff_elo = elo_ind1 - elo_ind2

    return sigmoid(diff_elo)

def elo_function(data = NanoString_df, Nduels = 10, Nsamples = Nsamples, k = 50):
    """
    Comoutation of elo for all the individuals in the MI data based on their SES features.
    Returns a ndarray with the elo of all the iterations. The last column contains the final
    elo value. The initialisation of elo is set at 1000.

    Arguments:
        data : (pandas.DataFrame) the data containing the SES features of each individual. It should
               contain the tindividuals as rows and the SES features as columns
        Nduels : (int) [default = 10] number of duels the user wishs to create for each individuals.
                The higher, the more relevant. (Well in theory)
        Nsamples : (int) Number of rows of data.
        k : (float) Constant of gain rate. The higher k, the higher the gain/loss will be after each match
            Litterature adviuses to take it between 16 and 200. [default 50]
    
    Values:
        elo_counts : (ndarray) of shape (Nsamples, Nduels + 1) containing the updated elo after each duel (columns).
                     The final column give the final elo after all the duels.
    """

    elo_counts = np.zeros((Nsamples, Nduels + 1))
    elo_counts[:, 0] = np.array([1000 for i in range(Nsamples)])

    for duel in range(1, Nduels + 1):

        list_to_duel = [i for i in range(Nsamples)]
        #print(list_to_duel)
        #print(duel)
        #liste = []
        for i in range(Nsamples // 2):

            individual = np.random.choice(list_to_duel)
            list_to_duel.remove(individual)
            
            sample_to_duel = np.random.choice(list_to_duel)
            list_to_duel.remove(sample_to_duel)
            #print(individual, sample_to_duel)

            #liste.append(len(list_to_duel))

            game = match(individual1 = individual, individual2 = sample_to_duel, data = data)

            p = win_proba(individual1 = individual, individual2 = sample_to_duel, duel_step = duel, elo_mat = elo_counts)

            if game[0] > game[1]:
                elo_counts[individual, duel] = elo_counts[individual, duel - 1] + (1 - p)*k
                elo_counts[sample_to_duel, duel] = elo_counts[sample_to_duel, duel - 1] - (1 - p)*k
                # if p > 0.5:
                #    elo_counts[individual, duel] = elo_counts[individual, duel - 1] + (1 - p)*k
                #    elo_counts[sample_to_duel, duel] = elo_counts[sample_to_duel, duel - 1] - (1 - p)*k
                # else :
                #     elo_counts[individual, duel] = elo_counts[individual, duel - 1] +  p*k
                #     elo_counts[sample_to_duel, duel] = elo_counts[sample_to_duel, duel - 1] - p*k
            elif game[0] < game[1]:
                elo_counts[individual, duel] = elo_counts[individual, duel - 1] - (p)*k
                elo_counts[sample_to_duel, duel] = elo_counts[sample_to_duel, duel - 1] + (p)*k
                # if p > 0.5:
                #     elo_counts[individual, duel] = elo_counts[individual, duel - 1] - p*k
                #     elo_counts[sample_to_duel, duel] = elo_counts[sample_to_duel, duel - 1] + p*k
                # else :
                #     elo_counts[individual, duel] = elo_counts[individual, duel - 1] - (1 - p)*k
                #     elo_counts[sample_to_duel, duel] = elo_counts[sample_to_duel, duel - 1] + (1 - p)*k
            else :
                elo_counts[individual, duel] = elo_counts[individual, duel - 1]
                elo_counts[sample_to_duel, duel] = elo_counts[sample_to_duel, duel - 1]
        #print(liste)
    return elo_counts

nduels = 500
seed(34)
elo_counts = elo_function(data = NanoString_df, Nduels = nduels, Nsamples = Nsamples, k = 50)
print(elo_counts)

## Conversion to data frame
today = date.today()
d1 = today.strftime("%Y_%m_%d")

final_elos = elo_counts[:, -1]
final_elos = final_elos.reshape((final_elos.shape[0], 1))
print(final_elos.shape)
tmp = NanoString_df.to_numpy()
tmp = np.concatenate((tmp, final_elos), axis =1)
colnames = colnames + ["ELO"]
NanoString_df = pd.DataFrame(tmp, columns = colnames)
print(NanoString_df)

NanoString_df.to_csv(path_to_data + "20201221_MI_SES_Metadata_with_Elo_" + d1 +".csv", sep = "\t", header = True, index = False)



## Plots
plt.rcParams['text.usetex'] = True # LaTeX labeling
# Plot of Elo density
# print(elo_counts[:, -1])
# mean_elo = np.mean(elo_counts[:, -1])
# std_elo = np.std(elo_counts[:, -1])
# les_elos = np.arange(-2000, 4000, 5)
fig1 = plt.figure()
plt.hist(final_elos, bins = 20 ,density=True, alpha=0.7)
#plt.plot(les_elos, norm.pdf(les_elos, loc = mean_elo, scale = std_elo), label = r"\cal{N}(\mu_{elo}, \sigma^2_{elo})")
plt.ylabel(r"Density", fontsize = 16)
plt.xlabel(r"Elo", fontsize = 16)
plt.savefig("/Path/to/plots/elo_density_" + d1 + ".pdf")


# Plots of trajectories (evolution of elo of 10 individuals through the algorithm) :
fig2 = plt.figure()
plt.xlabel(r"Duels", fontsize = 16)
plt.ylabel(r"Elo", fontsize = 16)
plt.title(r"Evolution of Elo ranking through time")
seed(42)
samples_to_plot = np.random.choice(Nsamples, 10)
step_duels = np.arange(nduels + 1)
for i in range(10):
    plt.plot(step_duels, elo_counts[samples_to_plot[i],:])
plt.savefig("/Path/to/plots/elo_evolution_" + d1 + ".pdf")

elo_counts_sub = elo_counts[samples_to_plot, :]
df_elo_counts_sub = pd.DataFrame(elo_counts_sub)
df_elo_counts_sub.to_csv(path_to_data + "Elo_counts_used_for_plots.csv", sep = "\t", header = True, index = False)

# Plots proba function 
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

    # Move left y-axis and bottim x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
    # Remove upper and right boxes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
    # Place ticks
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
    # Compute proba function    
test_sigmoid = np.linspace(-1000, 1000, 1600)
f_sigmoid = sigmoid(test_sigmoid)
    # Plot 
plt.axhline(y=1.0, color="black", linestyle="--", alpha = 0.4)
plt.plot(test_sigmoid, f_sigmoid, label = r"$p(t) = \frac{1}{1 + 10^{-0.0025 t}}$")
plt.xlabel(r"Elo difference", fontsize=16)
plt.ylabel(r"$p$", fontsize = 16)
plt.legend(fontsize=14, loc = "upper right", bbox_to_anchor=(0.5, 0.95))
plt.savefig("/Path/to/plots/proba_function_" + d1 + ".pdf")
plt.show()