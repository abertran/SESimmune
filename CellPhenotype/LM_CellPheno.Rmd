---
title: "LM_ELO_CellCountsMFI"
output: html_document
date: "2024-03-25"
---

# Loading Packages

```{r}
library(tidyr)
library(dplyr)
library(ggplot2)
```

```{r}
# Set Sex colors
colors.sex <- c("female" = "#CD534CFF", 
                "male" = "#0073C2FF")

colors.sex.both <- c("female" = "#CD534CFF", 
                      "male" = "#0073C2FF",
                      "both" = "green")

```


# Loading Data

## Setting the root directory

```{r setup, echo=FALSE}
# Change Path to your working directory
knitr::opts_knit$set(root.dir = '/Path/to/folder/')
# No warnings or supplementary message in the html 
knitr::opts_chunk$set(warning = FALSE, message = FALSE)
```

## Import Data

This data frame is generated in the script `DataPrep.R`.

```{r}
df.cell.counts <- read.csv("./Data/CellCountsMFI_stim_sex_age_elo.csv", header = T, sep = "\t")
rownames(df.cell.counts) <- df.cell.counts$SUBJID
```

# Performing Linear Models

```{r}
colnames(df.cell.counts) <- gsub("_", " ", colnames(df.cell.counts))
colnames(df.cell.counts) <- gsub("pos", "+", colnames(df.cell.counts))
cell_phenotype <- colnames(df.cell.counts)[6:ncol(df.cell.counts)]
N <- length(cell_phenotype)

res <- data.frame(cell_pheno = rep(cell_phenotype, 2),
                  Sex = c(rep("female", N), rep("male", N)))

sexList <- c("female", "male")#, "both")

for (cell in cell_phenotype){
  
  df.to.fit <- df.cell.counts %>% filter(Age > 30) %>% select(c(Age, CMV, ELO, SEX, !!rlang::sym(cell)))
  colnames(df.to.fit)[ncol(df.to.fit)] <- "pheno"
  
  for (sex in sexList){
    
    if (sex == 'both'){
      fit <- df.to.fit %>% lm(pheno ~ SEX + Age + CMV + ELO, data = .)
    }
    else{
      fit <- df.to.fit %>% filter(SEX == sex) %>% 
                           lm(pheno ~ Age + CMV + ELO, data = .)
    }
    
    res[res$cell_pheno == cell & res$Sex == sex, "beta0"] <- summary(fit)$coefficients[,'Estimate'][["(Intercept)"]]
    res[res$cell_pheno == cell & res$Sex == sex, "betaElo"] <- summary(fit)$coefficients[,'Estimate'][["ELO"]]
    res[res$cell_pheno == cell & res$Sex == sex, "pval"] <- summary(fit)$coefficients[,'Pr(>|t|)'][["ELO"]]
    
  }
}

res <- res %>% group_by(Sex) %>% 
               mutate(padj = p.adjust(pval, method = "fdr")) %>% 
               mutate(log.padj = -log10(padj)) %>% 
               arrange(log.padj) %>% 
               ungroup()
res$cell_pheno <- factor(res$cell_pheno, levels = unique(res$cell_pheno))

res %>% ggplot(aes(x = cell_pheno, y = log.padj, fill = Sex)) +
        geom_bar(stat = "identity", position = position_dodge()) +
        scale_fill_manual(values = colors.sex) +
        theme_classic() +
        geom_hline(yintercept = -log10(0.05), linetype = "dashed") +
        xlab("Immune Cell Phenotype") + 
        ylab("-log10(FDR)") +
        theme(axis.text.y = element_text(size = 3)) +
        coord_flip()
ggsave("./Plots/Review/LM_CellElo/Scree_pval_lm_flip.pdf", width = 5, height = 7)

res %>% ggplot(aes(x = cell_pheno, y = log.padj, fill = Sex)) +
        geom_bar(stat = "identity", position = position_dodge()) +
        scale_fill_manual(values = colors.sex) +
        theme_classic() +
        geom_hline(yintercept = -log10(0.05), linetype = "dashed") +
        xlab("Immune Cell Phenotype") + 
        ylab("-log10(FDR)") +
        theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1, size = 3), 
              legend.position = "top")
ggsave("./Plots/Review/LM_CellElo/Scree_pval_lm.pdf", width = 7, height = 5)


```

# Making distinct plots for MFI and Counts separately

```{r}
# Create 2 distinct data frames, one for the MFI and one for the counts
## MFI
### Need to extract pheno containing the pattern MFI in the name (using the grepl function)
res.MFI <- res %>% filter(grepl("MFI", cell_pheno))
## Counts 
### Just need to extract the pheno that do not contain "MFI" in the name
res.N <- res %>% filter(!grepl("MFI", cell_pheno))

# Plot for MFI
res.MFI %>% ggplot(aes(x = cell_pheno, y = log.padj, fill = Sex)) +
            geom_bar(stat = "identity", position = position_dodge()) +
            scale_fill_manual(values = colors.sex) +
            theme_classic() +
            geom_hline(yintercept = -log10(0.05), linetype = "dashed") +
            xlab("Immune Cell Phenotype") + 
            ylab(latex2exp::TeX(r"($-log_{10}(FDR)$)")) +
            theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1, size = 3), 
                  legend.position = "top")
ggsave("./Plots/Review/LM_CellElo/Scree_pval_lm_MFI_ratio.pdf", width = 7, height = 5)

# Plot for Counts
res.N %>% ggplot(aes(x = cell_pheno, y = log.padj, fill = Sex)) +
            geom_bar(stat = "identity", position = position_dodge()) +
            scale_fill_manual(values = colors.sex) +
            theme_classic() +
            geom_hline(yintercept = -log10(0.05), linetype = "dashed") +
            xlab("Immune Cell Phenotype") + 
            ylab(latex2exp::TeX(r"($-log_{10}(FDR)$)")) +
            theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1, size = 3), 
                  legend.position = "top")
ggsave("./Plots/Review/LM_CellElo/Scree_pval_lm_Counts_ratio.pdf", width = 7, height = 5)
```
# Withour Ratios

```{r}
df.cell.pheno <- df.cell.counts %>% select(!c(grep("ratio", names(df.cell.counts))))
df.cell.counts <- df.cell.pheno
colnames(df.cell.counts) <- gsub("_", " ", colnames(df.cell.counts))
colnames(df.cell.counts) <- gsub("pos", "+", colnames(df.cell.counts))
cell_phenotype <- colnames(df.cell.counts)[6:ncol(df.cell.counts)]
N <- length(cell_phenotype)

res <- data.frame(cell_pheno = rep(cell_phenotype, 2),
                  Sex = c(rep("female", N), rep("male", N)))

sexList <- c("female", "male")#, "both")

for (cell in cell_phenotype){
  
  df.to.fit <- df.cell.counts %>% filter(Age > 30) %>% select(c(Age, CMV, ELO, SEX, !!rlang::sym(cell)))
  colnames(df.to.fit)[ncol(df.to.fit)] <- "pheno"
  
  for (sex in sexList){
    
    if (sex == 'both'){
      fit <- df.to.fit %>% lm(pheno ~ SEX + Age + CMV + ELO, data = .)
    }
    else{
      fit <- df.to.fit %>% filter(SEX == sex) %>% 
                           lm(pheno ~ Age + CMV + ELO, data = .)
    }
    
    res[res$cell_pheno == cell & res$Sex == sex, "beta0"] <- summary(fit)$coefficients[,'Estimate'][["(Intercept)"]]
    res[res$cell_pheno == cell & res$Sex == sex, "betaElo"] <- summary(fit)$coefficients[,'Estimate'][["ELO"]]
    res[res$cell_pheno == cell & res$Sex == sex, "pval"] <- summary(fit)$coefficients[,'Pr(>|t|)'][["ELO"]]
    
  }
}

res <- res %>% group_by(Sex) %>% 
               mutate(padj = p.adjust(pval, method = "fdr")) %>% 
               mutate(log.padj = -log10(padj)) %>% 
               arrange(log.padj) %>% 
               ungroup()
res$cell_pheno <- factor(res$cell_pheno, levels = unique(res$cell_pheno))

# Create 2 distinct data frames, one for the MFI and one for the counts
## MFI
### Need to extract pheno containing the pattern MFI in the name (using the grepl function)
res.MFI <- res %>% filter(grepl("MFI", cell_pheno))
res.MFI <- res.MFI %>% arrange(Sex) %>% arrange(log.padj)
res.MFI$cell_pheno <- factor(res.MFI$cell_pheno, levels = unique(res.MFI$cell_pheno, fromLast = T))
## Counts 
### Just need to extract the pheno that do not contain "MFI" in the name
res.N <- res %>% filter(!grepl("MFI", cell_pheno))
res.N <- res.N %>% arrange(Sex) %>% arrange(log.padj)
res.N$cell_pheno <- factor(res.N$cell_pheno, levels = unique(res.N$cell_pheno, fromLast = T))
# Plot for MFI
res.MFI %>% ggplot(aes(x = cell_pheno, y = log.padj, fill = Sex)) +
            geom_bar(stat = "identity", position = position_dodge()) +
            scale_fill_manual(values = colors.sex) +
            theme_classic() +
            geom_hline(yintercept = -log10(0.05), linetype = "dashed") +
            xlab("Immune Cell Phenotype") + 
            ylab(latex2exp::TeX(r"($-log_{10}(FDR)$)")) +
            theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1, size = 3), 
                  legend.position = "top")
ggsave("./Plots/Review/LM_CellElo/Scree_pval_lm_MFI.pdf", width = 7, height = 5)

res.N %>% ggplot(aes(x = cell_pheno, y = log.padj, fill = Sex)) +
            geom_bar(stat = "identity", position = position_dodge()) +
            scale_fill_manual(values = colors.sex) +
            theme_classic() +
            geom_hline(yintercept = -log10(0.05), linetype = "dashed") +
            xlab("Immune Cell Phenotype") + 
            ylab(latex2exp::TeX(r"($-log_{10}(FDR)$)")) +
            theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1, size = 3), 
                  legend.position = "top")
ggsave("./Plots/Review/LM_CellElo/Scree_pval_lm_Counts.pdf", width = 7, height = 5)


```

