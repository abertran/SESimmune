library(dplyr)
library(tidyr)
library(ggplot2)
library(ggpubr)

setwd("/Path/to/folder")

# Defining the function to perform chi2 and plot the associated balloon plots
chi2 <- function(x,
                 
                 row.as.variable = NULL,
                 col.as.variable = NULL,
                 
                 refactor.col = F,
                 factors.col = NULL,
                 refactor.row = F,
                 factors.row = NULL, 
                 
                 graph = T, 
                 graph.save = NULL, 
                 
                 save.height = 3.6, 
                 save.width = 3.3, 
                 save.units = 'cm',
                 
                 balloon.x = 5,
                 balloon.y = 5,
                 balloon.width = 5, 
                 balloon.height = 5, 
                 balloon.default.unit = "cm",
                 balloon.color = "red",
                 
                 category.row.x = 1.5,
                 category.row.y = 4.9,
                 category.row.width = 2.5, 
                 category.row.height = 5, 
                 category.row.default.unit = "cm",
                 category.row.text.size = 7,
                 color.distrib.row = "blueviolet", 
                 
                 category.col.x = 5,
                 category.col.y = 8.2,
                 category.col.width = 5, 
                 category.col.height = 2, 
                 category.col.default.unit = "cm",
                 category.col.text.size = 7,
                 color.distrib.col = "blueviolet", 
                 
                 results.x = 5,
                 results.y = 1,
                 results.width = 2, 
                 results.height = 1, 
                 results.default.unit = "cm",
                 
                 y.lab = NULL, 
                 x.lab = NULL
){
  
  chisq <- chisq.test(x)
  
  x[, row.as.variable] <- rownames(x)
  x <- x %>% relocate(!!row.as.variable, 
                      .before = where(is.numeric))
  colnames(x)[1] <- "Category"
  
  if (refactor.row){
    if (!is.null(factors.row)){
      x$Category <- factor(x$Category,
                           levels = factors.row,
                           ordered = T)
    }
  }
  
  x <- x %>% gather("Condition", "N", 2:ncol(x))
  
  if (refactor.col){
    if (!is.null(factors.col)){
      x$Condition <- factor(x$Condition,
                            levels = factors.col,
                            ordered = T)
    }
  }
  
  plt <- x %>% ggplot(aes(x = Condition, y = Category)) +
    geom_point(aes(size = abs(N)), color = balloon.color) + 
    theme_linedraw() +
    scale_x_discrete(position="top") + guides(size = "none") + 
    theme(axis.title.y = element_blank(),
          axis.ticks.y = element_blank(),
          axis.text.y = element_blank(), 
          axis.title.x = element_blank(),
          axis.ticks.x = element_blank(),
          axis.text.x = element_blank())
  
  distrib.condition <- x %>% group_by(Condition) %>% summarise(Tot = sum(N)) %>% 
    ggplot(aes(x = Condition, y = Tot)) + 
    geom_bar(stat="identity", fill = color.distrib.col) +
    theme_pubr() + 
    scale_x_discrete(position="top") +
    xlab(x.lab) +
    theme(axis.title.y = element_blank(),
          axis.line.y = element_blank(),
          axis.ticks.y = element_blank(),
          axis.text.y = element_blank(),
          axis.text.x = element_text(size = category.col.text.size))
  
  distrib.category <- x %>% group_by(Category) %>% summarise(Tot = sum(N)) %>% 
    ggplot(aes(x = Category, y = Tot)) + 
    geom_bar(stat="identity", fill = color.distrib.row) +
    theme_pubr() + 
    scale_y_reverse() +
    coord_flip() +
    xlab(y.lab) +
    theme(axis.title.x = element_blank(),
          axis.line.x = element_blank(),
          axis.ticks.x = element_blank(),
          axis.text.x = element_blank(), 
          axis.text.y = element_text(size = category.row.text.size))
  
  results <- ggplot() + ggtitle(latex2exp::TeX(r"($\chi ^2~test$)"), paste0("p-val = ", formatC(chisq$p.value, digits = 2)))
  
  
  if (!is.null(graph.save)){
    pdf(graph.save,
        height = save.height, width = save.width)
    dev.size(units = save.units)
    grid::grid.newpage()
    print(plt,
          vp = grid::viewport(x = balloon.x, y = balloon.y, width = balloon.width, 
                              height = balloon.height, default.units = balloon.default.unit))
    print(distrib.category, 
          vp = grid::viewport(x = category.row.x, y = category.row.y, width = category.row.width, 
                              height = category.row.height, default.units = category.row.default.unit))
    print(distrib.condition, 
          vp = grid::viewport(x = category.col.x, y = category.col.y, width = category.col.width, 
                              height = category.col.height, default.units = category.col.default.unit))
    print(results, 
          vp = grid::viewport(x = results.x, y = results.y, width = results.width, height = results.height, default.units = results.default.unit))
    dev.off()
  }
  
  if (graph){
    grid::grid.newpage()
    print(plt,
          vp = grid::viewport(x = balloon.x, y = balloon.y, width = balloon.width, 
                              height = balloon.height, default.units = balloon.default.unit))
    print(distrib.category, 
          vp = grid::viewport(x = category.row.x, y = category.row.y, width = category.row.width, 
                              height = category.row.height, default.units = category.row.default.unit))
    print(distrib.condition, 
          vp = grid::viewport(x = category.col.x, y = category.col.y, width = category.col.width, 
                              height = category.col.height, default.units = category.col.default.unit))
    print(results, 
          vp = grid::viewport(x = results.x, y = results.y, width = results.width, height = results.height, default.units = results.default.unit))
  }
  
  return (chisq)
}

time.date <- substr(Sys.time(), 1, 10)

# Loading the data
df <- read.csv("./Data/LabExMI_Nanostring_9Stim_925Donors_Data_2019_12_03_ELO_LogTransformed.tsv", 
               sep = '\t', 
               header=T)
  

df <- df %>% filter(stimuli.nr == "Null")

# Create 3 categories for low, mid and high SES

df <- df %>% arrange(ELO)

for (i in 1:nrow(df)){ 
  if (i <= nrow(df) / 3){
    df[i, "Rank"] <- "Low"
  }
  else if ( (i <= 2 * nrow(df) / 3) && (i > nrow(df) / 3) ){
    df[i, "Rank"] <- "Mid"
  }
  else {
    df[i, "Rank"] <- "High"
  }
}

df <- df %>% relocate(Rank, .after = ELO) %>% arrange(SUBJID)

df$Rank <- factor(df$Rank, levels = c("Low", "Mid", "High"))

# Select variable of interest

df <- df %>% select(c(SUBJID, SEX, AGE.V0, CMVsero, Smoking, ELO, Rank))

df$Smoking <- factor(df$Smoking, levels = c("Non Smoker", "Ex Smoker", "Smoker"))

################################################################################
#######                     CMV serostatus and SES                       #######
################################################################################

### Comparison for males

sex <- "male"

##### First we need to format the data frame into a contingency table.
##### This table keeps the count of CMV pos and CMV neg (columns) for each 
##### SES category (rows).

to_test_chi_cmv_male <- df %>% filter(SEX == sex) %>% # keep male samples
                           filter(AGE.V0 >= 30) %>% # keep age > 30 y.o.
                           select(c(SEX, Rank, CMVsero)) %>% # keep the SES rank and CMV serostatus for the test
                           mutate(`is_Neg` = as.numeric(CMVsero == "N")) %>% # Count the CMV pos and neg  
                           mutate(`is_Pos` = as.numeric(CMVsero == "P")) %>% 
                           group_by(Rank) %>% 
                           summarise(`Negative` = sum(`is_Neg`),
                                     `Positive` = sum(`is_Pos`)
                                     )

rownames.tmp <- to_test_chi_cmv_male$Rank
to_test_chi_cmv_male <- to_test_chi_cmv_male %>% select(!c(Rank))
rownames(to_test_chi_cmv_male) <- rownames.tmp

##### Perform the chi2 and plot the results using the chi2 function

chi2(to_test_chi_cmv_male, 
     row.as.variable = "SES",
     col.as.variable = "CMV", 
     
     refactor.col = T,
     factors.col = c("Negative", "Positive"), 
     refactor.row = T, 
     factors.row = c("Low", "Mid", "High"),
     
     graph = T,
     # graph.save = paste0("./Plots/SES_features/chi2/Chi2_CMV_SES_", sex,"_", time.date,  ".pdf"),
     
     balloon.color = "#0073C2",
     
     x.lab = "CMV Serostatus",
     y.lab = "Socioeconomic Status"
)


### Comparison for males

sex <- "female"

##### First we need to format the data frame into a contingency table.
##### This table keeps the count of CMV pos and CMV neg (columns) for each 
##### SES category (rows).

to_test_chi_cmv_female <- df %>% filter(SEX == sex) %>% # keep male samples
  filter(AGE.V0 >= 30) %>% # keep age > 30 y.o.
  select(c(SEX, Rank, CMVsero)) %>% # keep the SES rank and CMV serostatus for the test
  mutate(`is_Neg` = as.numeric(CMVsero == "N")) %>% # Count the CMV pos and neg  
  mutate(`is_Pos` = as.numeric(CMVsero == "P")) %>% 
  group_by(Rank) %>% 
  summarise(`Negative` = sum(`is_Neg`),
            `Positive` = sum(`is_Pos`)
  )

rownames.tmp <- to_test_chi_cmv_female$Rank
to_test_chi_cmv_female <- to_test_chi_cmv_female %>% select(!c(Rank))
rownames(to_test_chi_cmv_female) <- rownames.tmp

##### Perform the chi2 and plot the results using the chi2 function

chi2(to_test_chi_cmv_female, 
     row.as.variable = "SES",
     col.as.variable = "CMV", 
     
     refactor.col = T,
     factors.col = c("Negative", "Positive"), 
     refactor.row = T, 
     factors.row = c("Low", "Mid", "High"),
     
     graph = T,
     # graph.save = paste0("./Plots/SES_features/chi2/Chi2_CMV_SES_", sex,"_", time.date,  ".pdf"),
     
     balloon.color = "#CD534C",
     
     x.lab = "CMV Serostatus",
     y.lab = "Socioeconomic Status"
)

################################################################################
#######             CMV Serostatus and SES - Log Regression             ########
################################################################################
df$CMVsero <- factor(df$CMVsero, levels = c("P", "N")) 

# For male
sex <- "male"

fitlog <- df %>% filter(AGE.V0 >= 30) %>% # exlude donors younger than 30
  filter(SEX == sex) %>% 
  glm(CMVsero ~ ELO + AGE.V0, data = ., family = "binomial") # perform log regression model adjusting by age

summary(fitlog)

# For female
sex <- "female"

fitlog <- df %>% filter(AGE.V0 >= 30) %>% # exlude donors younger than 30
  filter(SEX == sex) %>% 
  glm(CMVsero ~ ELO + AGE.V0, data = ., family = "binomial") # perform log regression model adjusting by age

summary(fitlog)

################################################################################
#######                     Smoking Status and SES                       #######
################################################################################

### Comparison for males

sex <- "male"

##### First we need to format the data frame into a contingency table.
##### This table keeps the count of CMV pos and CMV neg (columns) for each 
##### SES category (rows).

to_test_chi_smoking_male <- df %>% filter(SEX == sex) %>% # keep male samples
  filter(AGE.V0 >= 30) %>% # keep age > 30 y.o.
  select(c(SEX, Rank, Smoking)) %>% # keep the SES rank and CMV serostatus for the test
  mutate(`is_NonSmoker` = as.numeric(Smoking == "Non Smoker")) %>% # Count the proportion of smoking status
  mutate(`is_ExSmoker` = as.numeric(Smoking == "Ex Smoker")) %>% 
  mutate(`is_Smoker` = as.numeric(Smoking == "Smoker")) %>% 
  group_by(Rank) %>% 
  summarise(`Non Smoker` = sum(`is_NonSmoker`),
            `Ex Smoker` = sum(`is_ExSmoker`),
            `Smoker` = sum(`is_Smoker`)
            )

rownames.tmp <- to_test_chi_smoking_male$Rank
to_test_chi_smoking_male <- to_test_chi_smoking_male %>% select(!c(Rank))
rownames(to_test_chi_smoking_male) <- rownames.tmp

##### Perform the chi2 and plot the results using the chi2 function

chi2(to_test_chi_smoking_male, 
     row.as.variable = "SES",
     col.as.variable = "CMV", 
     
     refactor.col = T,
     factors.col = c("Non Smoker", "Ex Smoker", "Smoker"),
     refactor.row = T, 
     factors.row = c("Low", "Mid", "High"),
     
     graph = T,
     # graph.save = paste0("./Plots/SES_features/chi2/Chi2_Smoking_SES_", sex,"_", time.date,  ".pdf"),
     
     balloon.color = "#0073C2",
     
     x.lab = "Smoking status",
     y.lab = "Socioeconomic Status"
)

### Comparison for males

sex <- "female"

##### First we need to format the data frame into a contingency table.
##### This table keeps the count of CMV pos and CMV neg (columns) for each 
##### SES category (rows).

to_test_chi_smoking_female <- df %>% filter(SEX == sex) %>% # keep male samples
  filter(AGE.V0 >= 30) %>% # keep age > 30 y.o.
  select(c(SEX, Rank, Smoking)) %>% # keep the SES rank and CMV serostatus for the test
  mutate(`is_NonSmoker` = as.numeric(Smoking == "Non Smoker")) %>% # Count the proportion of smoking status
  mutate(`is_ExSmoker` = as.numeric(Smoking == "Ex Smoker")) %>% 
  mutate(`is_Smoker` = as.numeric(Smoking == "Smoker")) %>% 
  group_by(Rank) %>% 
  summarise(`Non Smoker` = sum(`is_NonSmoker`),
            `Ex Smoker` = sum(`is_ExSmoker`),
            `Smoker` = sum(`is_Smoker`)
  )

rownames.tmp <- to_test_chi_smoking_female$Rank
to_test_chi_smoking_female <- to_test_chi_smoking_female %>% select(!c(Rank))
rownames(to_test_chi_smoking_female) <- rownames.tmp

##### Perform the chi2 and plot the results using the chi2 function

chi2(to_test_chi_smoking_female, 
     row.as.variable = "SES",
     col.as.variable = "CMV", 
     
     refactor.col = T,
     factors.col = c("Non Smoker", "Ex Smoker", "Smoker"),
     refactor.row = T, 
     factors.row = c("Low", "Mid", "High"),
     
     graph = T,
     # graph.save = paste0("./Plots/SES_features/chi2/Chi2_Smoking_SES_", sex,"_", time.date,  ".pdf"),
     
     balloon.color = "#CD534C",
     
     x.lab = "Smoking Status",
     y.lab = "Socioeconomic Status"
)
