# Impact of Socioeconomic status on healthy immune responses in humans

<img src="./SESGraphicalAbstract.png"/>

This repository contains pieces of code that were used to generate the results of the paper "Impact of sciocioeconomic status on healthy immune responses in humans" Bertrand *et al.*, *Immunology & Cell Biology* 2024.
To rerun these pieces of code, one may have to change the root directory at the beginning of the code and adjust it to one's folder architecture.

This repository is organized as follows:
  - *CRP*
    - `CRP_SES.Rmd` : R markdown to compare the level of CRP with the SES of MI donors. 
  - *CMV_Smoking_SES*
    - `CMV_Smoking_SEStests.R` : R script to comapre the CMV serostatus and the smoking behavior of MI donors to their SES.
  - *CellPhenotype*
    - `LM_CellPheno.Rmd` : R markdown to comapre cell phenotypes (counts and MFI) of the donors to their SES through lm.
  - *Elo*
    - `EloComputation.py` : Python script to compute the Elo score of the MI donors based on their SES.
    - `SES_features_Distribution.R` : R script to visualize the distribution of SES features accross MI donors
  - *RandomForests*
    - *ImportanceOfCellCounts*
      - `RF_CellCounts_both.R` : R script to perform random forests models to compare the gene expression level to the cell counts including both male and female
      - `RF_CellCounts_female.R` : R script to perform random forests models to compare the gene expression level to the cell counts for female
      - `RF_CellCounts_male.R` : R script to perform random forests models to compare the gene expression level to the cell counts for male
    - *ImportanceOfSES*
      - `RF_SESfeatures_vs_GeneExp_adjustCellCounts.R` : R script to perform random forests models to compare the gene expression level to SES features and adjusting by biological variables (top cell counts, CRP level, CMV serostatus).
      - `RF_ResultsAnalysis.Rmd` : R markdown to visualize the results of the RF models

Python script ran on Python/3.8.2
R scripts ran on R/4.2.2