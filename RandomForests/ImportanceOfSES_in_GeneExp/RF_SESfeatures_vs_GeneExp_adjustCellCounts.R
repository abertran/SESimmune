library(dplyr)
library(tidyr)
library(randomForest)

setwd("/path/to/directory")

set.seed(4269) # setting a random seed for reproducibility

df <- read.csv("./data/NanoString_df_for_random_forests.csv",
               sep = "\t",
               header = T) %>% select(!c(Education, Income, Housing, CSP)) %>% filter(Age > 30)

cell.data <- read.csv("./data/NanoString_CellCounts_stim_sex.csv",
                      sep = "\t",
                      header = T)

number.cells <- 10

n.features <- ncol(df %>% select(SUBJID:Stimuli) %>% select(!c(SUBJID, Stimuli))) + number.cells
mt <- max(floor(n.features/3), 1)

rf.fit <- function(gene, stim, df_to_use, sex, ntrees = 1000, mtry = mt, nfeat = n.features){ # Function to fit the random forest models
  
  df_used <- df_to_use %>% # Selecting data for a given stimulation
    filter(Stimuli == stim) %>% 
    select(!c(Stimuli))
  
  if (sex != "both"){
    df_used <- df_used %>% # Filtering sex and removing the sex variable
      filter(SEX == sex) %>% 
      select(!SEX)
    
    df_used <- df_used[, c(2:nfeat, which(colnames(df_used) == gene))] # Selecting only the features of interest
    
    colnames(df_used) <-  c(colnames(df_used[1:(nfeat - 1)]), "GENE")
    
    fit <- randomForest(GENE ~ ., # Fitting the model
                        data = df_used,
                        ntree = ntrees,
                        mtry = mtry, 
                        importance = T, 
                        na.action = na.omit)
  }
  else{
    df_used <- df_used[, c(2:(nfeat+1), which(colnames(df_used) == gene))] # Selecting the features of interest
    
    colnames(df_used) <- c(colnames(df_used[1:nfeat]), "GENE")
    
    fit <- randomForest(GENE ~ .,  # Fitting the model
                        data = df_used,
                        ntree = ntrees,
                        mtry = mtry, 
                        importance = T, 
                        na.action = na.omit)
  }
  
  res.imp <- list()
  
  res.imp[["importance"]] <- as.data.frame(importance(fit)[, "%IncMSE"]) # as.data.frame(varImpPlot(fit, type = 1))
  colnames(res.imp[["importance"]]) <- c("%IncMSE")
  res.imp[["ntrees"]] <- fit$ntree
  
  return(res.imp)
}

sexlist <- c("female", "male", "both")
stimulist <- unique(df$Stimuli)
geneList <- colnames(df)[which(colnames(df) == "ABCB1"):ncol(df)]

t1 <- Sys.time()

# Perform RF models on each sex/stim pair

for (sex in sexlist){
  
  for (stim in stimulist){
    print(c(sex, stim))
    
    df.stock <- data.frame(genes = geneList) # Create a data frame to keep the info of the models' results
    rownames(df.stock) <- df.stock$genes
    
    res.celldata <- read.csv(paste0("./results/CellCounts/FeaturesImportance_", sex, "_", stim, ".csv"),
                             sep = "\t", 
                             header = T) # import the result of the RF models for the importance of cell counts
    
    res.celldata <- res.celldata %>% 
      summarise(across(
        where(is.numeric),
        ~ sum(.x, na.rm =T) / nrow(res.celldata)
      )
      ) %>% 
      gather("Feature", "Mean %IncMSE", 1:(ncol(res.celldata)-1)) %>% 
      filter(Feature != "SEX") %>% 
      arrange(desc(`Mean %IncMSE`)) # Order cell types by importance to explain gene expression data 
    
    cell.to.integrate <- res.celldata$Feature[1:10] # Keep the top 10
    cell.data.to.merge <- cell.data[, c("SUBJID", cell.to.integrate)] %>% distinct(SUBJID,.keep_all = T) 
    
    # merge the top 10 with the data frame of gene expression and SES feature
    df.to.fit <- merge(df, cell.data.to.merge, 
                       by.x = "SUBJID", by.y = "SUBJID") %>% 
      relocate(all_of(cell.to.integrate), .before = Stimuli)
    
    # Perform RF model for each gene
    for (gene in geneList){
      fit <- 0
      fit <-  rf.fit(gene = gene, stim = stim, df_to_use = df.to.fit, sex = sex)
      
      imp <- fit$importance
      
      df.stock[gene, 2:(nrow(imp)+1)] <- imp$`%IncMSE` # Add the %IncMSE in the data frame 
    }
    
    colnames(df.stock)[2:(nrow(imp)+1)] <- rownames(imp)
    # Save results in csv
    write.table(df.stock, 
                paste0("./results/SES/FeaturesImportance_", sex, "_", stim, ".csv"),
                sep = "\t",
                col.names = T, 
                row.names = F)
  }
  
}

print(Sys.time() - t1)