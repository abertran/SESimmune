library(dplyr)
library(randomForest)

setwd("/path/to/folder/")

set.seed(4269) # setting a random seed for reproducibility

df <- read.csv("./data/NanoString_CellCounts_stim_sex.csv",
               sep = "\t", header = T) %>% select(!total)

n.features <- ncol(df %>% select(CD16hi:CXCR5pos))
mt <- max(floor(n.features/3), 1)

rf.fit <- function(gene, stim, df_to_use, sex, ntrees = 1000, mtry = mt, nfeat = n.features){ # Function to fit the random forest models
  
  df_used <- df_to_use %>% # Selecting data for a given stimulation
    filter(Stimuli == stim) %>% 
    select(!c(Stimuli))
  
  if (sex != "both"){
    df_used <- df_used %>% # Filtering sex and removing the sex variable
      filter(SEX == sex) %>% 
      select(!SEX)
    
    df_used <- df_used[, c(2:(nfeat+1), which(colnames(df_used) == gene))] # Selecting only the features of interest
    
    colnames(df_used) <-  c(colnames(df_used[1:(nfeat)]), "GENE")
    
    fit <- randomForest(GENE ~ ., # Fitting the model
                        data = df_used,
                        ntree = ntrees,
                        mtry = mtry, 
                        importance = T, 
                        na.action = na.omit)
  }
  else{
    df_used <- df_used[, c(2:(nfeat+2), which(colnames(df_used) == gene))] # Selecting the features of interest
    
    colnames(df_used) <- c(colnames(df_used[1:(nfeat+1)]), "GENE")
    
    fit <- randomForest(GENE ~ .,  # Fitting the model
                        data = df_used,
                        ntree = ntrees,
                        mtry = mtry, 
                        importance = T, 
                        na.action = na.omit)
  }
  
  res.imp <- list()
  
  res.imp[["importance"]] <- as.data.frame(importance(fit)[, "%IncMSE"]) # as.data.frame(varImpPlot(fit, type = 1))
  colnames(res.imp[["importance"]]) <- c("%IncMSE")
  res.imp[["ntrees"]] <- fit$ntree
  
  return(res.imp)
  
}

stimulist <- unique(df$Stimuli)
geneList <- colnames(df)[which(colnames(df) == "ABCB1"):ncol(df)]

t1 <- Sys.time()

sex <- "male"

for (stim in stimulist){
  print(c(sex, stim))
  
  df.stock <- data.frame(genes = geneList)
  rownames(df.stock) <- df.stock$genes
  
  for (gene in geneList){
    fit <- 0
    fit <-  rf.fit(gene = gene, stim = stim, df_to_use = df, sex = sex)
    
    imp <- fit$importance
    
    df.stock[gene, 2:(nrow(imp)+1)] <- imp$`%IncMSE`
  }
  
  colnames(df.stock)[2:(nrow(imp)+1)] <- rownames(imp)
  write.table(df.stock, 
              paste0("./results/CellCounts/FeaturesImportance_", sex, "_", stim, ".csv"),
              sep = "\t",
              col.names = T, 
              row.names = F)
}

print(Sys.time() - t1)